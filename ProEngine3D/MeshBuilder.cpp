#include "MeshBuilder.h"

using namespace glm;

unique_ptr<Mesh> MeshBuilder::Plane()
{
	Mesh *mesh = new Mesh;

	mesh->GetVertices().assign(PlaneVertices.begin(), PlaneVertices.end());
	mesh->GetIndices().assign(PlaneIndices.begin(), PlaneIndices.end());

	mesh->SetMaterialId(0);

	mesh->Build();

	return unique_ptr<Mesh>(mesh);
}

unique_ptr<Mesh> MeshBuilder::Cube()
{
	Mesh *mesh = new Mesh;

	mesh->GetVertices().assign(CubeVertices.begin(), CubeVertices.end());
	mesh->GetIndices().assign(CubeIndices.begin(), CubeIndices.end());

	mesh->SetMaterialId(0);

	mesh->Build();

	return unique_ptr<Mesh>(mesh);
}

unique_ptr<InstancedMesh> MeshBuilder::InstancedPoint()
{
	InstancedMesh *mesh = new InstancedMesh;

	mesh->GetVertices().push_back(Vertex());

	mesh->SetMaterialId(0);

	mesh->Build();

	return unique_ptr<InstancedMesh>(mesh);
}

unique_ptr<InstancedMesh> MeshBuilder::InstancedCube()
{
	InstancedMesh *mesh = new InstancedMesh;

	mesh->GetVertices().assign(CubeVertices.begin(), CubeVertices.end());
	mesh->GetIndices().assign(CubeIndices.begin(), CubeIndices.end());

	mesh->SetMaterialId(0);

	mesh->Build();

	return unique_ptr<InstancedMesh>(mesh);
}

const vector<uint32_t> MeshBuilder::PlaneIndices = {
	0, 1, 2,
	2, 3, 0
};

const vector<Vertex> MeshBuilder::PlaneVertices = {
	/*position							color			texcoords					normal						tangent	*/
	{ vec4(-1.0f, -1.0f, 0.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, -1.0f, 0.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f,  1.0f, 0.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f,  1.0f, 0.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(1.0f, 0.0f, 0.0f) }
};

const vector<Vertex> MeshBuilder::CubeVertices = {
	/*position							color			texcoords					normal						tangent	*/
	// FRONT
	{ vec4(-1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f,  1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f,  1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	// LEFT
	{ vec4(-1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f),	vec3(0.0f, 0.0f, -1.0f) },
	{ vec4(-1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f),	vec3(0.0f, 0.0f, -1.0f) },
	{ vec4(-1.0f,  1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f),	vec3(0.0f, 0.0f, -1.0f) },
	{ vec4(-1.0f,  1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f),	vec3(0.0f, 0.0f, -1.0f) },
	// RIGHT
	{ vec4(1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f) },
	{ vec4(1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f) },
	{ vec4(1.0f,  1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f) },
	{ vec4(1.0f,  1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, 1.0f) },
	// TOP
	{ vec4(-1.0f, 1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, 1.0f, 1.0f, 0.0f),		vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, 1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f, 1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(-1.0f, 0.0f, 0.0f) },
	// BOTTOM
	{ vec4(-1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(0.0f, -1.0f, 0.0f),	vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, -1.0f, 0.0f),	vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(0.0f, -1.0f, 0.0f),	vec3(-1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f, -1.0f, 1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(0.0f, -1.0f, 0.0f),	vec3(-1.0f, 0.0f, 0.0f) },
	// BACK
	{ vec4(1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, -1.0f),	vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f, -1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 0.0f, 0.0f),		vec3(0.0f, 0.0f, -1.0f),	vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(-1.0f, 1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(1.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, -1.0f),	vec3(1.0f, 0.0f, 0.0f) },
	{ vec4(1.0f, 1.0f, -1.0f, 0.0f),	vec4(1.0f),		vec3(0.0f, 1.0f, 0.0f),		vec3(0.0f, 0.0f, -1.0f),	vec3(1.0f, 0.0f, 0.0f) },
};

const vector<uint32_t> MeshBuilder::CubeIndices = {
	0, 1, 2,
	2, 3, 0,

	4, 5, 6,
	6, 7, 4,

	8, 9, 10,
	10, 11, 8,

	12, 13, 14,
	14, 15, 12,

	16, 17, 18,
	18, 19, 16,

	20, 21, 22,
	22, 23, 20
};
