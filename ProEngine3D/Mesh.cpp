#include "Mesh.h"

Mesh::~Mesh()
{
	DeleteBuild();
}

vector<Vertex> &Mesh::GetVertices()
{
	return mVertices;
}

vector<uint32_t>& Mesh::GetIndices()
{
	return mIndices;
}

void * Mesh::GetVertexData()
{
	return &mVertices[0];
}

void * Mesh::GetIndexData()
{
	return &mIndices[0];
}

size_t Mesh::GetNumVertices()
{
	return mVertices.size();
}

size_t Mesh::GetNumIndices()
{
	return mIndices.size();
}

uint32_t Mesh::GetMaterialId()
{
	return mMaterialId;
}

void Mesh::SetMaterialId(uint32_t id)
{
	mMaterialId = id;
}

void Mesh::SetRenderMode(ERenderMode mode)
{
	mRenderMode = mode;
}

Mesh::ERenderMode Mesh::GetRenderMode()
{
	return mRenderMode;
}

bool Mesh::HasIndexedVertices()
{
	return (mIndices.size() > 0);
}

void Mesh::Build()
{
	DeleteBuild();

	glGenVertexArrays(1, &mVAO);
	glGenBuffers(1, &mVBO);
	glGenBuffers(1, &mEBO);

	BindVertexArray();

	glBindBuffer(GL_ARRAY_BUFFER, mVBO);
	glBufferData(GL_ARRAY_BUFFER, GetNumVertices() * sizeof(Vertex), GetVertexData(), GL_STATIC_DRAW);

	if (HasIndexedVertices())
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetNumIndices() * sizeof(uint32_t), GetIndexData(), GL_STATIC_DRAW);
	}

	glEnableVertexAttribArray(PositionAttribIndex);
	glVertexAttribPointer(PositionAttribIndex, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	glEnableVertexAttribArray(ColorAttribIndex);
	glVertexAttribPointer(ColorAttribIndex, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(4 * sizeof(float)));

	glEnableVertexAttribArray(TexCoordAttribIndex);
	glVertexAttribPointer(TexCoordAttribIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(8 * sizeof(float)));

	glEnableVertexAttribArray(NormalAttribIndex);
	glVertexAttribPointer(NormalAttribIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(11 * sizeof(float)));

	glEnableVertexAttribArray(TangentAttribIndex);
	glVertexAttribPointer(TangentAttribIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(14 * sizeof(float)));

	UnbindVertexArray();
}

void Mesh::UpdateBuild()
{
	BindVertexArray();
	glBindBuffer(GL_ARRAY_BUFFER, mVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, GetNumVertices() * sizeof(Vertex), GetVertexData());
}

void Mesh::Render()
{
	BindVertexArray();

	if (HasIndexedVertices())
		glDrawElements(mRenderMode, (GLsizei)GetNumIndices(), GL_UNSIGNED_INT, 0);
	else
		glDrawArrays(mRenderMode, 0, (GLsizei)GetNumVertices());

	UnbindVertexArray();
}

void Mesh::DeleteBuild()
{
	glDeleteBuffers(1, &mEBO);
	glDeleteBuffers(1, &mVBO);
	glDeleteVertexArrays(1, &mVAO);
}
