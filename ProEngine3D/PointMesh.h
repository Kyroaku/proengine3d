#pragma once

#include <vector>

#include "config.h"

#include "Vertex.h"

using std::vector;

class PointMesh {
private:
	vector<PointVertex> mVertices;
	GLuint mVAO = 0, mVBO = 0;

	static const GLuint PositionAttribIndex = 0;
	static const GLuint ColorAttribIndex = 1;

public:
	PRO_API ~PointMesh();

	PRO_API vector<PointVertex> &GetVertices();
	PRO_API void *GetVertexData();
	PRO_API void *GetIndexData();
	PRO_API size_t GetNumVertices();

	PRO_API virtual void Build();
	PRO_API virtual void UpdateBuild();

	PRO_API virtual void Render();

protected:
	PRO_API inline void BindVertexArray() { glBindVertexArray(mVAO); }
	PRO_API inline void UnbindVertexArray() { glBindVertexArray(0); }
};