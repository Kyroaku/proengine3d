#include "Model.h"

#include <iostream>
#include <3rdparty/assimp/Importer.hpp>
#include <3rdparty/assimp/Exporter.hpp>
#include <3rdparty/assimp/scene.h>
#include <3rdparty/assimp/postprocess.h>

#ifdef _WIN64
#	ifdef _DEBUG
#		pragma comment(lib, "assimp/lib/x64/Debug/assimp-vc140-mt")
#	else
#		pragma comment(lib, "assimp/lib/x64/Release/assimp-vc140-mt")
#	endif
#else
#	ifdef _DEBUG
#		pragma comment(lib, "assimp/lib/x86/Debug/assimp-vc140-mt")
#	else
#		pragma comment(lib, "assimp/lib/x86/Release/assimp-vc140-mt")
#	endif
#endif

using namespace std;

//using std::shared_ptr;
//using std::make_shared;

void Model::LoadNode(aiNode *ainode, shared_ptr<ModelNode> node)
{
	//cout << "Node: " << ainode->mName.C_Str() << endl;
	//cout << "NumChildrens: " << ainode->mNumChildren << endl;
	//cout << "NumMeshes: " << ainode->mNumMeshes << endl;

	/* Load data */
	for (unsigned int i = 0; i < ainode->mNumMeshes; i++) {
		node->AddMesh(ainode->mMeshes[i]);
	}
	glm::mat4 transformation;
	memcpy(&transformation, &ainode->mTransformation, sizeof(transformation));
	node->SetTransformation(transpose(transformation));

	/* Load childrens */
	for (unsigned int i = 0; i < ainode->mNumChildren; i++) {
		shared_ptr<ModelNode> child(new ModelNode);
		node->AddChildren(child);
		child->SetParent(node);
		LoadNode(ainode->mChildren[i], child);
	}
}

void Model::RenderNode(shared_ptr<ModelNode> node, Shader *shader, glm::mat4 transformation /* = glm::mat4(1.0f) */)
{
	if (node == nullptr)
		return;

	glm::mat4 globalTransform = transformation * node->GetTransformation();
	shader->UniformMatrix4fv("uModelMatrix", false, &globalTransform[0][0]);
	shader->UniformMatrix4fv("uTextureMatrix", false, &node->GetTextureTransformation()[0][0]);
	for (auto mesh : node->GetMeshes()) {
		auto &mat = mMaterials.at(mMeshes.at(mesh)->GetMaterialId());
		int hasMap[16] = { 0 };
		for (auto texture : mat->mTextures) {
#			ifdef PRO_DEBUG_HARD
			if (texture.first >= 16) {
				cout << "[Debug] Error: Texture index too high." << endl;
				__debugbreak();
			}
#			endif
			texture.second->Bind(texture.first);
			hasMap[texture.first] = 1;
		}
		shader->Uniform3fv("uMaterial.mAmbientColor", &mat->mColorAmbient[0]);
		shader->Uniform3fv("uMaterial.mDiffuseColor", &mat->mColorDiffuse[0]);
		shader->Uniform3fv("uMaterial.mSpecularColor", &mat->mColorSpecular[0]);
		shader->Uniform1f("uMaterial.mOpacity", mat->mOpacity);
		shader->Uniform1f("uMaterial.mShininess", mat->mShininess);
		shader->Uniform1f("uMaterial.mShininessStrength", mat->mShininessStrength);
		shader->Uniform1f("uMaterial.mReflectivity", mat->mReflectivity);
		shader->Uniform1i("uHasDiffuseMap", hasMap[Material::eDiffuseMap]);
		shader->Uniform1i("uHasNormalMap", hasMap[Material::eNormalMap]);
		shader->Uniform1i("uHasSpecularMap", hasMap[Material::eSpecularMap]);
		shader->Uniform1i("uHasDisplacementMap", hasMap[Material::eDisplacementMap]);
		shader->Uniform1i("uHasAmbientOcclusionMap", hasMap[Material::eAmbientOcclusionMap]);
		shader->Uniform1i("uHasCubeMap", hasMap[Material::eCubeMap]);
		mMeshes.at(mesh)->Render();
	}
	for (auto child : node->GetChildrens()) {
		RenderNode(child, shader, globalTransform);
	}
}

Model::Model()
{
	mRootNode = make_shared<ModelNode>();
}

void Model::LoadFromFile(string path)
{
	string directory = path.substr(0, path.find_last_of("/\\")) + '/';

	/* Load assimp scene from file */
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(
		path,
		aiPostProcessSteps::aiProcess_Triangulate
		//| aiPostProcessSteps::aiProcess_SortByPType
		| aiPostProcessSteps::aiProcess_CalcTangentSpace
		| aiPostProcessSteps::aiProcess_GenNormals
		| aiPostProcessSteps::aiProcess_GenUVCoords
		| aiPostProcessSteps::aiProcess_FlipUVs
	);

	if (!scene) {
		cout << "[Model] Error: " << importer.GetErrorString() << endl;
		return;
	}

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
	cout << "Begin scene {" << endl;
	PRINT_VALUE(scene->mNumMaterials);
	PRINT_VALUE(scene->mNumMeshes);
#endif
#pragma endregion Model info

	/* Parse meshes */
	for (unsigned int m_i = 0; m_i < scene->mNumMeshes; m_i++) {
		unique_ptr<Mesh> mesh(new Mesh);
		aiMesh *aimesh = scene->mMeshes[m_i];

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
		cout << "Begin mesh {" << endl;
		PRINT_VALUE(aimesh->mName.C_Str());
		PRINT_VALUE(aimesh->mNumVertices);
		PRINT_VALUE(aimesh->mNumFaces);
		PRINT_VALUE(aimesh->mNumAnimMeshes);
		PRINT_VALUE(aimesh->mNumBones);
		PRINT_VALUE(aimesh->mNumUVComponents[0]);
		PRINT_VALUE(aimesh->mMaterialIndex);
		PRINT_VALUE(aimesh->mMethod);
		PRINT_VALUE(aimesh->mPrimitiveTypes);
#endif
#pragma endregion Model info

		/* Parse vertices */
		for (unsigned int v_i = 0; v_i < aimesh->mNumVertices; v_i++) {
			Vertex v;
			memset(&v, 0, sizeof(v)); // set zeros in vertex.
			if (aimesh->HasPositions())
				v.mPosition = glm::vec4(aimesh->mVertices[v_i].x, aimesh->mVertices[v_i].y, aimesh->mVertices[v_i].z, 1.0f);
			if (aimesh->HasVertexColors(0))
				v.mColor = glm::vec4(aimesh->mColors[0][v_i].r, aimesh->mColors[0][v_i].g, aimesh->mColors[0][v_i].b, aimesh->mColors[0][v_i].a);
			if (aimesh->HasTextureCoords(0))
				v.mTexCoord = glm::vec3(aimesh->mTextureCoords[0][v_i].x, aimesh->mTextureCoords[0][v_i].y, aimesh->mTextureCoords[0][v_i].z);
			if (aimesh->HasNormals())
				v.mNormal = glm::vec3(aimesh->mNormals[v_i].x, aimesh->mNormals[v_i].y, aimesh->mNormals[v_i].z);
			if (aimesh->HasTangentsAndBitangents()) {
				v.mTangent = glm::vec3(aimesh->mTangents[v_i].x, aimesh->mTangents[v_i].y, aimesh->mTangents[v_i].z);
			}
			mesh->GetVertices().push_back(v);
		}
		/* Parse faces */
		for (unsigned int f_i = 0; f_i < aimesh->mNumFaces; f_i++) {
			for (unsigned int i_i = 0; i_i < aimesh->mFaces[f_i].mNumIndices; i_i++)
				mesh->GetIndices().push_back(aimesh->mFaces[f_i].mIndices[i_i]);
		}
		mesh->SetMaterialId(aimesh->mMaterialIndex);

		mesh->Build();
		mMeshes.push_back(std::move(mesh));

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
		cout << "End mesh" << endl << "}" << endl;
#endif
#pragma endregion Model info
	}

	/* Parse materials */
	for (unsigned int m_i = 0; m_i < scene->mNumMaterials; m_i++) {
		unsigned int t_count;
		aiMaterial *aimaterial = scene->mMaterials[m_i];
		unique_ptr<Material> material(new Material);

		aiColor3D color = aiColor3D(1.0f, 1.0f, 1.0f);
		aiString str = aiString("unnamed");
		float real = 1.0f;
		int integer = 0;

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
		cout << "Begin material {" << endl;
		PRINT_VALUE(aimaterial->mNumAllocated);
		PRINT_VALUE(aimaterial->mNumProperties);
		if (aimaterial->Get(AI_MATKEY_NAME, str) == aiReturn_SUCCESS)
			cout << "[Debug] name = " << str.C_Str() << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_AMBIENT, color) == aiReturn_SUCCESS)
			cout << "[Debug] ambient = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color) == aiReturn_SUCCESS)
			cout << "[Debug] diffuse = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_SPECULAR, color) == aiReturn_SUCCESS)
			cout << "[Debug] specular = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_EMISSIVE, color) == aiReturn_SUCCESS)
			cout << "[Debug] emissive = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_REFLECTIVE, color) == aiReturn_SUCCESS)
			cout << "[Debug] reflective = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_OPACITY, real) == aiReturn_SUCCESS)
			cout << "[Debug] opacity = " << real << endl;
		if (aimaterial->Get(AI_MATKEY_SHININESS, real) == aiReturn_SUCCESS)
			cout << "[Debug] shininess = " << real << endl;
		if (aimaterial->Get(AI_MATKEY_SHININESS_STRENGTH, real) == aiReturn_SUCCESS)
			cout << "[Debug] shininess strength = " << real << endl;
		if (aimaterial->Get(AI_MATKEY_REFLECTIVITY, real) == aiReturn_SUCCESS)
			cout << "[Debug] reflectivity = " << real << endl;
		if (aimaterial->Get(AI_MATKEY_COLOR_TRANSPARENT, color) == aiReturn_SUCCESS)
			cout << "[Debug] transparent = [" << color.r << ", " << color.g << ", " << color.b << "]" << endl;
		if (aimaterial->Get(AI_MATKEY_ENABLE_WIREFRAME, integer) == aiReturn_SUCCESS)
			cout << "[Debug] wireframe = " << (integer != 0 ? "true" : "false") << endl;
		if (aimaterial->Get(AI_MATKEY_TWOSIDED, integer) == aiReturn_SUCCESS)
			cout << "[Debug] twosided = " << (integer != 0 ? "true" : "false") << endl;
#endif
#pragma endregion Model info

		/* Parse material parameters */
		if (aimaterial->Get(AI_MATKEY_NAME, str) == aiReturn_SUCCESS)
			material->mName = str.C_Str();
		if (aimaterial->Get(AI_MATKEY_COLOR_AMBIENT, color) == aiReturn_SUCCESS)
			material->mColorAmbient = glm::vec3(color.r, color.g, color.b);
		if (aimaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color) == aiReturn_SUCCESS)
			material->mColorDiffuse = glm::vec3(color.r, color.g, color.b);
		if (aimaterial->Get(AI_MATKEY_COLOR_SPECULAR, color) == aiReturn_SUCCESS)
			material->mColorSpecular = glm::vec3(color.r, color.g, color.b);
		if (aimaterial->Get(AI_MATKEY_OPACITY, real) == aiReturn_SUCCESS)
			material->mOpacity = real;
		if (aimaterial->Get(AI_MATKEY_SHININESS, real) == aiReturn_SUCCESS)
			material->mShininess = real;
		if (aimaterial->Get(AI_MATKEY_SHININESS_STRENGTH, real) == aiReturn_SUCCESS)
			material->mShininessStrength = real;
		if (aimaterial->Get(AI_MATKEY_REFLECTIVITY, real) == aiReturn_SUCCESS)
			material->mReflectivity = real;

		/* Parse diffuse textures */
		t_count = aimaterial->GetTextureCount(aiTextureType_DIFFUSE);
		for (unsigned int t_i = 0; t_i < t_count; t_i++) {
			aiString path;
			aimaterial->GetTexture(aiTextureType_DIFFUSE, t_i, &path, NULL, NULL, NULL, NULL, NULL); // Very disabled reading tex path.
			material->AddTexture(Material::eDiffuseMap, make_shared<Texture>(directory + path.C_Str()));
		}
		/* Parse normal textures */
		t_count = aimaterial->GetTextureCount(aiTextureType_NORMALS);
		for (unsigned int t_i = 0; t_i < t_count; t_i++) {
			aiString path;
			aimaterial->GetTexture(aiTextureType_NORMALS, t_i, &path, NULL, NULL, NULL, NULL, NULL); // Very disabled reading tex path.
			material->AddTexture(Material::eNormalMap, make_shared<Texture>(directory + path.C_Str()));
		}
		mMaterials.push_back(move(material));

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
		cout << "End material" << endl << "}" << endl;
#endif
#pragma endregion Model info
	}

	/* Load nodes */
	LoadNode(scene->mRootNode, mRootNode);

#pragma region Debug
#if PRO_MODEL_DEBUG_INFO == 1
	cout << "End scene" << endl << "}" << endl;
#endif
#pragma endregion Model info
}

void Model::Render(Shader *shader, glm::mat4 transformation /* = glm::mat4(1.0f) */)
{
	if (mRootNode)
		RenderNode(mRootNode, shader, transformation);
}

vector<unique_ptr<Mesh>>& Model::GetMeshes()
{
	return mMeshes;
}

vector<unique_ptr<Material>>& Model::GetMaterials()
{
	return mMaterials;
}

shared_ptr<ModelNode> Model::GetRootNode()
{
	return mRootNode;
}
