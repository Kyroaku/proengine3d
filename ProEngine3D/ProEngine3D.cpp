#include "Logger.h"

#include "ProEngine3D.h"

bool ProEngine3D::Init()
{
	glewExperimental = TRUE;
	GLenum err;
	if ((err = glewInit()) != GLEW_OK) {
		Logger::Error("GLEW Init failed (error: " + std::to_string(err) + ")");
		return false;
	}
	else {
		Logger::Success("GLEW Initialized.");
		return true;
	}
}
