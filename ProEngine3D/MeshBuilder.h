#pragma once

#include <memory>

#include "config.h"
#include "Mesh.h"
#include "InstancedMesh.h"

using std::unique_ptr;

class MeshBuilder {
public:
	PRO_API static unique_ptr<Mesh> Plane();
	PRO_API static unique_ptr<Mesh> Cube();
	PRO_API static unique_ptr<InstancedMesh> InstancedCube();
	PRO_API static unique_ptr<InstancedMesh> InstancedPoint();

private:
	static const vector<Vertex> PlaneVertices;
	static const vector<uint32_t> PlaneIndices;

	static const vector<Vertex> CubeVertices;
	static const vector<uint32_t> CubeIndices;
};