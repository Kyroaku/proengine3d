#include "Logger.h"

#include <iostream>

/* Console colors */
#include <Windows.h>

void Logger::Debug(std::string msg) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eCyan);
	std::cout << "[Debug] ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
	std::cout << msg << std::endl;
}

void Logger::Info(std::string msg) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eWhite);
	std::cout << "[Info] ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
	std::cout << msg << std::endl;
}

void Logger::Error(std::string msg) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eRed);
	std::cout << "[Error] ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
	std::cout << msg << std::endl;
}

void Logger::Warning(std::string msg) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eYellow);
	std::cout << "[Warning] ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
	std::cout << msg << std::endl;
}

void Logger::Success(std::string msg) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGreen);
	std::cout << "[OK] ";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), EColor::eGray);
	std::cout << msg << std::endl;
}