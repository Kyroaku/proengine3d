#include "Shader.h"

#include <iostream>
#include <fstream>

#include "Logger.h"

using std::ifstream;

Shader::Shader()
	: mProgram(0)
{
}

Shader::Shader(string shaderName)
{
	Create(shaderName);
}
Shader::~Shader()
{
	/* Delete shader program. Shaders will be detached automaticaly. */
	glDeleteProgram(mProgram);
}
unique_ptr<char[]> Shader::LoadSourceFromFile(std::string filename)
{
	ifstream file(filename);
	if (!file.is_open()) {
		return nullptr;
	}

	file.seekg(0, file.end);
	std::streamsize len = file.tellg();
	file.seekg(0, file.beg);

	unique_ptr<char[]> src(new char[(int)len + 1]);
	memset(src.get(), '\0', (size_t)len + 1);
	file.read(src.get(), len);
	src[(size_t)file.gcount()] = 0;

	file.close();

	return std::move(src);
}
void Shader::Create(string shaderName)
{
	GLuint vertexShader, fragmentShader, geometryShader, computeShader;
	char infoLog[1024];

	auto vertexShaderSrc = LoadSourceFromFile(shaderName + ".vs");
	auto fragmentShaderSrc = LoadSourceFromFile(shaderName + ".fs");
	auto geometryShaderSrc = LoadSourceFromFile(shaderName + ".gs");
	auto computeShaderSrc = LoadSourceFromFile(shaderName + ".cs");

	if (vertexShaderSrc != nullptr)
		Logger::Success(shaderName + ".vs");
	else
		Logger::Warning(shaderName + ".vs" + " not found");

	if (fragmentShaderSrc != nullptr)
		Logger::Success(shaderName + ".fs");
	else
		Logger::Warning(shaderName + ".fs" + " not found");

	if (geometryShaderSrc != nullptr)
		Logger::Success(shaderName + ".gs");

	mProgram = glCreateProgram();

	if (vertexShaderSrc) {
		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		const char *ptr = vertexShaderSrc.get();
		glShaderSource(vertexShader, 1, &ptr, 0);
		glCompileShader(vertexShader);
		glAttachShader(mProgram, vertexShader);
		glGetShaderInfoLog(vertexShader, 1024, 0, infoLog);
		glDeleteShader(vertexShader);
		if (strlen(infoLog) > 0)
			printf("VertexShader Log:\n%s\n", infoLog);
	}

	if (fragmentShaderSrc) {
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		const char *ptr = fragmentShaderSrc.get();
		glShaderSource(fragmentShader, 1, &ptr, 0);
		glCompileShader(fragmentShader);
		glAttachShader(mProgram, fragmentShader);
		glGetShaderInfoLog(fragmentShader, 1024, 0, infoLog);
		glDeleteShader(fragmentShader);
		if (strlen(infoLog) > 0)
			printf("FragmentShader Log:\n%s\n", infoLog);
	}

	if (geometryShaderSrc) {
		geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		const char *ptr = geometryShaderSrc.get();
		glShaderSource(geometryShader, 1, &ptr, 0);
		glCompileShader(geometryShader);
		glAttachShader(mProgram, geometryShader);
		glGetShaderInfoLog(geometryShader, 1024, 0, infoLog);
		glDeleteShader(geometryShader);
		if (strlen(infoLog) > 0)
			printf("GeometryShader Log:\n%s\n", infoLog);
	}

	if (computeShaderSrc) {
		computeShader = glCreateShader(GL_COMPUTE_SHADER);
		const char *ptr = computeShaderSrc.get();
		glShaderSource(computeShader, 1, &ptr, 0);
		glCompileShader(computeShader);
		glAttachShader(mProgram, computeShader);
		glGetShaderInfoLog(computeShader, 1024, 0, infoLog);
		glDeleteShader(computeShader);
		if (strlen(infoLog) > 0)
			printf("ComputeShader Log:\n%s\n", infoLog);
	}

	/*glBindAttribLocation(mProgram, 0, "a_Position");
	glBindAttribLocation(mProgram, 1, "a_Color");
	glBindAttribLocation(mProgram, 2, "a_TextureCoord");
	glBindAttribLocation(mProgram, 3, "a_Normal");
	glBindAttribLocation(mProgram, 4, "a_Tangent");
	glBindAttribLocation(mProgram, 5, "a_Binormal");*/

	glLinkProgram(mProgram);

	glGetProgramInfoLog(mProgram, 1024, 0, infoLog);
	if (strlen(infoLog) > 0)
		printf("ShaderProgram Log:\n%s\n", infoLog);
}
void Shader::Use()
{
	glUseProgram(mProgram);
}

void Shader::Uniform1i(string loc, int v)
{
	glUniform1i(glGetUniformLocation(mProgram, loc.c_str()), v);
}
void Shader::Uniform2i(string loc, int v1, int v2)
{
	glUniform2i(glGetUniformLocation(mProgram, loc.c_str()), v1, v2);
}
void Shader::Uniform3i(string loc, int v1, int v2, int v3)
{
	glUniform3i(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3);
}
void Shader::Uniform4i(string loc, int v1, int v2, int v3, int v4)
{
	glUniform4i(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3, v4);
}
void Shader::Uniform2iv(string loc, int *v, int count)
{
	glUniform2iv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform3iv(string loc, int *v, int count)
{
	glUniform3iv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform4iv(string loc, int *v, int count)
{
	glUniform4iv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}

void Shader::Uniform1ui(string loc, unsigned int v)
{
	glUniform1ui(glGetUniformLocation(mProgram, loc.c_str()), v);
}
void Shader::Uniform2ui(string loc, unsigned int v1, unsigned int v2)
{
	glUniform2ui(glGetUniformLocation(mProgram, loc.c_str()), v1, v2);
}
void Shader::Uniform3ui(string loc, unsigned int v1, unsigned int v2, unsigned int v3)
{
	glUniform3ui(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3);
}
void Shader::Uniform4ui(string loc, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int v4)
{
	glUniform4ui(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3, v4);
}
void Shader::Uniform2uiv(string loc, unsigned int *v, int count)
{
	glUniform2uiv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform3uiv(string loc, unsigned int *v, int count)
{
	glUniform3uiv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform4uiv(string loc, unsigned int *v, int count)
{
	glUniform4uiv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}

void Shader::Uniform1f(string loc, float v)
{
	glUniform1f(glGetUniformLocation(mProgram, loc.c_str()), v);
}
void Shader::Uniform2f(string loc, float v1, float v2)
{
	glUniform2f(glGetUniformLocation(mProgram, loc.c_str()), v1, v2);
}
void Shader::Uniform3f(string loc, float v1, float v2, float v3)
{
	glUniform3f(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3);
}
void Shader::Uniform4f(string loc, float v1, float v2, float v3, float v4)
{
	glUniform4f(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3, v4);
}
void Shader::Uniform2fv(string loc, float *v, int count)
{
	glUniform2fv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform3fv(string loc, float *v, int count)
{
	glUniform3fv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform4fv(string loc, float *v, int count)
{
	glUniform4fv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}

void Shader::Uniform1d(string loc, double v)
{
	glUniform1d(glGetUniformLocation(mProgram, loc.c_str()), v);
}
void Shader::Uniform2d(string loc, double v1, double v2)
{
	glUniform2d(glGetUniformLocation(mProgram, loc.c_str()), v1, v2);
}
void Shader::Uniform3d(string loc, double v1, double v2, double v3)
{
	glUniform3d(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3);
}
void Shader::Uniform4d(string loc, double v1, double v2, double v3, double v4)
{
	glUniform4d(glGetUniformLocation(mProgram, loc.c_str()), v1, v2, v3, v4);
}
void Shader::Uniform2dv(string loc, double *v, int count)
{
	glUniform2dv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform3dv(string loc, double *v, int count)
{
	glUniform3dv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}
void Shader::Uniform4dv(string loc, double *v, int count)
{
	glUniform4dv(glGetUniformLocation(mProgram, loc.c_str()), count, v);
}

void Shader::UniformMatrix2x3fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix2x3fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix2x4fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix2x4fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3x2fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix3x2fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3x4fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix3x4fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4x2fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix4x2fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4x3fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix4x3fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix2fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix2fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix3fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4fv(string loc, bool transpose, float *v, int count)
{
	glUniformMatrix4fv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}

void Shader::UniformMatrix2x3dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix2x3dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix2x4dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix2x4dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3x2dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix3x2dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3x4dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix3x4dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4x2dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix4x2dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4x3dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix4x3dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix2dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix2dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix3dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix3dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}
void Shader::UniformMatrix4dv(string loc, bool transpose, double *v, int count)
{
	glUniformMatrix4dv(glGetUniformLocation(mProgram, loc.c_str()), count, transpose, v);
}

void Shader::UniformLight(string loc, Light & light)
{
	switch (light.mType) {
	case Light::eDirectional:
		Uniform3fv(loc + ".mAmbientColor", &light.mAmbientColor[0]);
		Uniform3fv(loc + ".mDiffuseColor", &light.mDiffuseColor[0]);
		Uniform3fv(loc + ".mSpecularColor", &light.mSpecularColor[0]);
		Uniform3fv(loc + ".mVector", &light.mDirection[0]);
		break;

	case Light::ePoint:
		Uniform3fv(loc + ".mAmbientColor", &light.mAmbientColor[0]);
		Uniform3fv(loc + ".mDiffuseColor", &light.mDiffuseColor[0]);
		Uniform3fv(loc + ".mSpecularColor", &light.mSpecularColor[0]);
		Uniform3fv(loc + ".mVector", &light.mPosition[0]);
		Uniform1f(loc + ".mConstAttenuation", light.mConstAttenuation);
		Uniform1f(loc + ".mLinearAttenuation", light.mLinearAttenuation);
		Uniform1f(loc + ".mQuadraticAttenuation", light.mQuadraticAttenuation);
		break;
	}
}
