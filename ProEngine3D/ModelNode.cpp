#include "ModelNode.h"

vector<uint32_t>& ModelNode::GetMeshes()
{
	return mMeshes;
}

vector<shared_ptr<ModelNode>>& ModelNode::GetChildrens()
{
	return mChildrens;
}

shared_ptr<ModelNode> ModelNode::GetParent()
{
	return mParent;
}

glm::mat4 ModelNode::GetTransformation()
{
	return mTransformation;
}

glm::mat4 ModelNode::GetTextureTransformation()
{
	return mTextureTransformation;
}

void ModelNode::SetParent(shared_ptr<ModelNode> parent)
{
	mParent = parent;
}

void ModelNode::SetTransformation(glm::mat4 transformation)
{
	mTransformation = transformation;
}

void ModelNode::SetTextureTransformation(glm::mat4 transformation)
{
	mTextureTransformation = transformation;
}

void ModelNode::AddMesh(uint32_t mesh)
{
	mMeshes.push_back(mesh);
}

void ModelNode::AddChildren(shared_ptr<ModelNode> children)
{
	mChildrens.push_back(children);
}