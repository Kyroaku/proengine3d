#include "Bitmap.h"

#include <iostream>

#ifdef _WIN64
#	pragma comment(lib, "FreeImage/x64/FreeImage")
#else
#	pragma comment(lib, "FreeImage/x32/FreeImage")
#endif

Bitmap::Bitmap() :
	mBitmap(0)
{
}

Bitmap::Bitmap(string filename)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(filename.c_str());
	mBitmap = FreeImage_Load(fif, filename.c_str(), 0);
	FreeImage_FlipVertical(mBitmap);
	if (!mBitmap) {
		std::cout << "[Error] Bitmap: Image load error (" << filename << ")" << std::endl;
	}
}

Bitmap::Bitmap(uint8_t * data, int width, int height, int bpp)
{
	mBitmap = FreeImage_ConvertFromRawBits(
		(BYTE*)data,
		width, height,
		width * bpp / 8,
		bpp,
		FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK,
		false);
	if (!mBitmap) {
		std::cout << "[Error] Bitmap: Image create error (" << std::endl;
	}
}

Bitmap::~Bitmap()
{
	FreeImage_Unload(mBitmap);
}

int Bitmap::GetWidth() const
{
	return FreeImage_GetWidth(mBitmap);
}

int Bitmap::GetHeight() const
{
	return FreeImage_GetHeight(mBitmap);
}

int Bitmap::GetBpp() const
{
	return FreeImage_GetBPP(mBitmap);
}

char * Bitmap::GetData() const
{
	return (char*)FreeImage_GetBits(mBitmap);
}
