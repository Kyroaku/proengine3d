#include "PointMesh.h"


#include "Mesh.h"

PointMesh::~PointMesh()
{
	glDeleteBuffers(1, &mVBO);
	glDeleteVertexArrays(1, &mVAO);
}

vector<PointVertex> &PointMesh::GetVertices()
{
	return mVertices;
}

void *PointMesh::GetVertexData()
{
	return &mVertices[0];
}

size_t PointMesh::GetNumVertices()
{
	return mVertices.size();
}

void PointMesh::Build()
{
	glGenVertexArrays(1, &mVAO);
	glGenBuffers(1, &mVBO);

	BindVertexArray();

	glBindBuffer(GL_ARRAY_BUFFER, mVBO);
	glBufferData(GL_ARRAY_BUFFER, GetNumVertices() * sizeof(PointVertex), GetVertexData(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(PositionAttribIndex);
	glVertexAttribPointer(PositionAttribIndex, 4, GL_FLOAT, GL_FALSE, sizeof(PointVertex), 0);

	glEnableVertexAttribArray(ColorAttribIndex);
	glVertexAttribPointer(ColorAttribIndex, 4, GL_FLOAT, GL_FALSE, sizeof(PointVertex), (void*)(4 * sizeof(float)));

	UnbindVertexArray();
}

void PointMesh::UpdateBuild()
{
	glBindVertexArray(mVAO);
	glBindBuffer(GL_ARRAY_BUFFER, mVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, GetNumVertices() * sizeof(PointVertex), GetVertexData());
}

void PointMesh::Render()
{
	BindVertexArray();
		
	glDrawArrays(GL_POINTS, 0, (GLsizei)GetNumVertices());

	UnbindVertexArray();
}
