#include "InstancedMesh.h"

#include <time.h>

InstancedMesh::~InstancedMesh()
{
	DeleteBuild();
}

std::vector<InstancedData>& InstancedMesh::GetInstancedData()
{
	return m_InstancedData;
}

void InstancedMesh::Build()
{
	Super::Build();

	glGenBuffers(1, &m_InstancedVBO);

	BindVertexArray();

	glBindBuffer(GL_ARRAY_BUFFER, m_InstancedVBO);
	glBufferData(GL_ARRAY_BUFFER, m_InstancedData.size() * sizeof(m_InstancedData[0]), m_InstancedData.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(InstancePositionAttribIndex);
	glVertexAttribPointer(InstancePositionAttribIndex, 3, GL_FLOAT, GL_FALSE, sizeof(InstancedData), (void*)offsetof(InstancedData, m_Position));
	glVertexAttribDivisor(InstancePositionAttribIndex, 1);

	glEnableVertexAttribArray(InstanceColorAttribIndex);
	glVertexAttribPointer(InstanceColorAttribIndex, 3, GL_FLOAT, GL_FALSE, sizeof(InstancedData), (void*)offsetof(InstancedData, Color));
	glVertexAttribDivisor(InstanceColorAttribIndex, 1);

	UnbindVertexArray();
}

void InstancedMesh::UpdateBuild()
{
	throw "Method not implemented!";
}

void InstancedMesh::Render(uint32_t instances)
{
	BindVertexArray();

	if (instances == 0)
		instances = m_InstancedData.size();

	if (HasIndexedVertices())
		glDrawElementsInstanced(GetRenderMode(), (GLsizei)GetNumIndices(), GL_UNSIGNED_INT, 0, instances);
	else
		glDrawArraysInstanced(GetRenderMode(), 0, (GLsizei)GetNumVertices(), instances);

	UnbindVertexArray();
}

void InstancedMesh::DeleteBuild()
{
	Super::DeleteBuild();

	glDeleteBuffers(1, &m_InstancedVBO);
}
