#pragma once

#include <vector>
#include <3rdparty/glm/glm.hpp>

#include "Mesh.h"

#include "config.h"

/**
 * Mesh for instanced rendering.
 */
class InstancedMesh : public Mesh
{
private:
	typedef Mesh Super;

private:
	GLuint m_InstancedVBO = 0;

	std::vector<InstancedData> m_InstancedData;

	static const GLuint InstancePositionAttribIndex = 10;
	static const GLuint InstanceColorAttribIndex = 11;

public:
	virtual ~InstancedMesh() override;

	PRO_API std::vector<InstancedData> &GetInstancedData();

	PRO_API virtual void Build() override;
	PRO_API virtual void UpdateBuild() override;
	PRO_API virtual void Render(uint32_t instances = 0);

protected:
	PRO_API virtual void DeleteBuild() override;
};