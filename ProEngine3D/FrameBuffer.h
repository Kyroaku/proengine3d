#pragma once

#include "config.h"

#include "Texture.h"

class FrameBuffer {
private:
	GLuint mFrameBuffer = 0;

public:
	PRO_API FrameBuffer();

	PRO_API void AttachColorBuffer(const Texture &texture);
	PRO_API void AttachDepthBuffer(const Texture &texture);
	PRO_API void AttachDepthStencilBuffer(glm::vec2 size, size_t samples = 0);

	PRO_API void Bind();
	/**
	 * Binds default framebuffer (0).
	 * 
	 * This method exists as non static method only for framebuffer bind/unbind
	 * consistency and it is not using any instance data.
	 */
	PRO_API void Unbind();
};