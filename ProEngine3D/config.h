#pragma once

/*	Config settings		*/

#ifdef _DEBUG

#define PRO_DEBUG				/* Enabling typical debugging options, defining helpers, etc */
#define PRO_DEBUG_HARD			/* Hard debug mode which stops debugging whenever something unexpected happens */

#define PRO_MODEL_DEBUG_INFO	0
#endif

/* End config settings	*/

/* DLL export */
#ifdef PROENGINE3D_EXPORTS
#	define PRO_API __declspec(dllexport)
#else
#	define PRO_API __declspec(dllimport)
#endif

/* Debug config */
#ifdef PRO_DEBUG
#include <iostream>
#define PRINT_VALUE(a) { std::cout << "[Debug] " << #a << " = " << a << std::endl; }
#endif

/* Includes */
#include <GL/glew.h>
#include <GL/GL.h>
#include <3rdparty/glm/glm.hpp>
#include <3rdparty/glm/gtc/matrix_transform.hpp>