#pragma once

#include <string>

#include "config.h"

class Logger {
private:
	enum EColor : unsigned char {
		eBlack = 0,
		eDarkBlue = 1,
		eDarkGreen = 2,
		eDarkCyan = 3,
		eDarkRed = 4,
		eDarkMagenta = 5,
		eDarkYellow = 6,
		eGray = 7,
		eDarkGray = 8,
		eBlue = 9,
		eGreen = 10,
		eCyan = 11,
		eRed = 12,
		eMagenta = 13,
		eYellow = 14,
		eWhite = 15
	};

public:
	PRO_API static void Debug(std::string msg);
	PRO_API static void Info(std::string msg);
	PRO_API static void Error(std::string msg);
	PRO_API static void Warning(std::string msg);
	PRO_API static void Success(std::string msg);
};