#include "Camera.h"

Camera::Camera()
	: mPosition(glm::vec3(0.0f))
	, mTargetPosition(glm::vec3(0.0f))
	, mViewMatrix(glm::mat4(1.0f))
	, mProjectionMatrix(glm::mat4(1.0f))
{
}

void Camera::SetPerspective(float fovy, float aspect, float near, float far)
{
	mFovy = glm::radians(fovy);
	mAspect = aspect;
	mNearPlane = near;
	mFarPlane = far;
	UpdateProjectionMatrix();
}

void Camera::SetOrthogonal(float width, float height, float depth)
{
	mProjectionMatrix = glm::orthoRH(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f, -depth / 2.0f, depth / 2.0f);
}

glm::mat4 Camera::GetMatrix() const
{
	return mProjectionMatrix * mViewMatrix;
}

glm::mat4 Camera::GetViewMatrix() const
{
	return mViewMatrix;
}

glm::mat4 Camera::GetProjectionMatrix() const
{
	return mProjectionMatrix;
}

void Camera::SetPosition(glm::vec3 pos)
{
	mPosition = pos;
	UpdateViewMatrix();
}

void Camera::SetLookAt(glm::vec3 pos)
{
	mTargetPosition = pos;
	UpdateViewMatrix();
}

glm::vec3 Camera::GetPosition() const
{
	return mPosition;
}

glm::vec3 Camera::GetLookAt() const
{
	return mTargetPosition;
}

glm::vec3 Camera::Deproject(glm::vec3 p)
{
	const auto deprojected_p = glm::vec3(glm::inverse(mProjectionMatrix) * glm::vec4(p, 1.0f));
	return glm::inverse(mViewMatrix) * glm::vec4(deprojected_p, 0.0f);
}

void Camera::UpdateViewMatrix()
{
	glm::vec3 z = normalize(mPosition - mTargetPosition);
	glm::vec3 x = normalize(glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), z));
	glm::vec3 y = glm::cross(z, x);
	mViewMatrix = glm::mat4(
		glm::vec4(x.x, y.x, z.x, 0.0f),
		glm::vec4(x.y, y.y, z.y, 0.0f),
		glm::vec4(x.z, y.z, z.z, 0.0f),
		glm::vec4(-glm::dot(x, mPosition), -glm::dot(y, mPosition), -glm::dot(z, mPosition), 1.0f)
	);
}

void Camera::UpdateProjectionMatrix()
{
	mProjectionMatrix = glm::perspective(mFovy, mAspect, mNearPlane, mFarPlane);
}
