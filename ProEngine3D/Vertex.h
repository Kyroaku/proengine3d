#pragma once

#include "config.h"

struct Vertex {
	glm::vec4 mPosition;
	glm::vec4 mColor;
	glm::vec3 mTexCoord;
	glm::vec3 mNormal;
	glm::vec3 mTangent;
};

struct InstancedData {
	glm::vec3 m_Position;
	glm::vec3 Color;
};

struct PointVertex {
	glm::vec3 m_Position;
	glm::vec3 Color;
};
