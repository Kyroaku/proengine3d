#pragma once

#include <string>

#include "config.h"

#include "Light.h"

using std::string;
using std::unique_ptr;

class Shader
{
private:
	GLuint mProgram;

public:
	PRO_API Shader();
	PRO_API Shader(string shaderName);
	PRO_API ~Shader();

	PRO_API void Create(string shaderName);
	PRO_API void Use();

	PRO_API void Uniform1i(string loc, int v);
	PRO_API void Uniform2i(string loc, int v1, int v2);
	PRO_API void Uniform3i(string loc, int v1, int v2, int v3);
	PRO_API void Uniform4i(string loc, int v1, int v2, int v3, int v4);
	PRO_API void Uniform2iv(string loc, int *v, int count = 1);
	PRO_API void Uniform3iv(string loc, int *v, int count = 1);
	PRO_API void Uniform4iv(string loc, int *v, int count = 1);

	PRO_API void Uniform1ui(string loc, unsigned int v);
	PRO_API void Uniform2ui(string loc, unsigned int v1, unsigned int v2);
	PRO_API void Uniform3ui(string loc, unsigned int v1, unsigned int v2, unsigned int v3);
	PRO_API void Uniform4ui(string loc, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int v4);
	PRO_API void Uniform2uiv(string loc, unsigned int *v, int count = 1);
	PRO_API void Uniform3uiv(string loc, unsigned int *v, int count = 1);
	PRO_API void Uniform4uiv(string loc, unsigned int *v, int count = 1);

	PRO_API void Uniform1f(string loc, float v);
	PRO_API void Uniform2f(string loc, float v1, float v2);
	PRO_API void Uniform3f(string loc, float v1, float v2, float v3);
	PRO_API void Uniform4f(string loc, float v1, float v2, float v3, float v4);
	PRO_API void Uniform2fv(string loc, float *v, int count = 1);
	PRO_API void Uniform3fv(string loc, float *v, int count = 1);
	PRO_API void Uniform4fv(string loc, float *v, int count = 1);

	PRO_API void Uniform1d(string loc, double v);
	PRO_API void Uniform2d(string loc, double v1, double v2);
	PRO_API void Uniform3d(string loc, double v1, double v2, double v3);
	PRO_API void Uniform4d(string loc, double v1, double v2, double v3, double v4);
	PRO_API void Uniform2dv(string loc, double *v, int count = 1);
	PRO_API void Uniform3dv(string loc, double *v, int count = 1);
	PRO_API void Uniform4dv(string loc, double *v, int count = 1);

	PRO_API void UniformMatrix2x3fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix2x4fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix3x2fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix3x4fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix4x2fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix4x3fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix2fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix3fv(string loc, bool transpose, float *v, int count = 1);
	PRO_API void UniformMatrix4fv(string loc, bool transpose, float *v, int count = 1);

	PRO_API void UniformMatrix2x3dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix2x4dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix3x2dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix3x4dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix4x2dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix4x3dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix2dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix3dv(string loc, bool transpose, double *v, int count = 1);
	PRO_API void UniformMatrix4dv(string loc, bool transpose, double *v, int count = 1);

	PRO_API void UniformLight(string loc, Light &light);

private:
	unique_ptr<char[]> LoadSourceFromFile(std::string filename);
};