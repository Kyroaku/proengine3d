#include "Logger.h"

#include "Texture.h"

Texture::Texture()
	: mTextureType(Texture::eTexture2D)
{
}

Texture::Texture(Texture::EType textureType, glm::vec2 size, EFormat format /* = eRGBA */, EDataType dataType /* = eUByte */, size_t samples /* = 0 */)
	: mTextureType(textureType)
{
	Create(textureType, size, format, dataType);
}

Texture::Texture(const Bitmap &bitmap, EType textureType /* = Texture::eTexture2D */)
	: mTextureType(textureType)
{
	Create(bitmap, textureType);
}

Texture::Texture(const string &path, EType textureType /* = Texture::eTexture2D */)
	: mTextureType(textureType)
{
	Create(path, textureType);
}

Texture::Texture(const BitmapList &bitmaps, EType textureType /* = Texture::eCubeMap */)
	: mTextureType(textureType)
{
	Create(bitmaps, textureType);
}

Texture::Texture(const PathList &pathes, EType textureType /* = Texture::eCubeMap */)
	: mTextureType(textureType)
{
	Create(pathes, textureType);
}

Texture::~Texture()
{
	glDeleteTextures(1, &mTexture);
}

void Texture::Create(EType textureType, glm::vec2 size, EFormat format /* = eRGBA */, EDataType dataType /* = eUByte */, GLsizei samples /* = 0 */)
{
	mTextureType = textureType;
	GenerateTexture(format);
	switch (textureType) {
	case Texture::eTexture2D:
		CreateTexture2D(size, format, dataType, samples);
		break;

	case Texture::eTexture2D_Multisample:
		CreateTexture2D_Multisample(size, format, dataType, samples);
		break;

	case Texture::eCubeMap:
		CreateCubeMap(size, format, dataType);
		break;

	default:
#		ifdef PRO_DEBUG_HARD
		std::cout << "Unsupported texture type: " << mTextureType << std::endl;
		__debugbreak();
#		endif
		break;
	}
}

void Texture::Create(const Bitmap & bitmap, EType textureType /* = Texture::eTexture2D */)
{
	mTextureType = textureType;
	GenerateTexture();
	switch (mTextureType) {
	case Texture::eTexture2D:
		CreateTexture2D(bitmap);
		break;

	case Texture::eCubeMap:
#		ifdef PRO_DEBUG_HARD
		std::cout << "CubeMap must be created by using list if bitmaps (strings)" << std::endl;
		__debugbreak();
#		endif
		break;

	default:
#		ifdef PRO_DEBUG_HARD
		std::cout << "Unsupported texture type: " << mTextureType << std::endl;
		__debugbreak();
#		endif
		break;
	}
	glGenerateMipmap(mTextureType);
}

void Texture::Create(const string &path, EType textureType /* = Texture::eTexture2D */)
{
	Create(Bitmap(path));
}

void Texture::Create(const Texture::BitmapList& bitmaps, EType textureType /* = Texture::eCubeMap */)
{
	mTextureType = textureType;
	GenerateTexture();
	switch (textureType) {
	case Texture::eCubeMap:
		CreateCubeMap(bitmaps);
		break;

	default:
#		ifdef PRO_DEBUG_HARD
		std::cout << "This texture type (" << mTextureType << ") doesn't support multiple bitmaps" << std::endl;
		__debugbreak();
#		endif
		break;
	}
}

void Texture::Create(const Texture::PathList& pathes, EType textureType /* = Texture::eCubeMap */)
{
	BitmapList bitmaps;
	for (size_t i = 0; i < pathes.size(); i++)
		bitmaps.push_back(make_pair(
			make_shared<Bitmap>(pathes.at(i).first), pathes.at(i).second
		));

	Create(bitmaps, textureType);
}

void Texture::Bind(int id /* = 0 */)
{
	glActiveTexture(GL_TEXTURE0 + id);
	glBindTexture(mTextureType, mTexture);
}

void Texture::BindImage(int id /* = 0 */)
{
	glActiveTexture(GL_TEXTURE0 + id);
	glBindImageTexture(0, mTexture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);
}

/// <summary>
/// Sets the type of the texture.
/// </summary>
/// <param name="target">The target.</param>
void Texture::SetTextureType(Texture::EType target)
{
	mTextureType = target;
}

GLuint Texture::GetHandle() const
{
	return mTexture;
}

Texture::EType Texture::GetTextureType() const
{
	return mTextureType;
}

void Texture::GenerateTexture(EFormat format /*  = Texture::eRGBA */)
{
	glEnable(mTextureType);
	glGenTextures(1, &mTexture);
	glBindTexture(mTextureType, mTexture);
	glTexParameteri(mTextureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(mTextureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(mTextureType, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(mTextureType, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(mTextureType, GL_TEXTURE_WRAP_R, GL_REPEAT);
	if (format == Texture::eDepth) {
		glTexParameteri(mTextureType, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(mTextureType, GL_TEXTURE_COMPARE_FUNC, GL_GREATER);
	}
}

void Texture::CreateTexture2D(glm::ivec2 size, EFormat format /* = eRGBA */, EDataType dataType /* = eUByte */, GLsizei samples /* = 0 */)
{
	glTexImage2D(
		GL_TEXTURE_2D,		/* Target */
		0,					/* Mipmap level */
		dataType == eFloat ? GL_RGBA32F : format,				/* Texture pixel format */
		(GLsizei)size.x,	/* Data width */
		(GLsizei)size.y,	/* Data height */
		0,					/* Border size */
		format,				/* Data format */
		dataType,			/* Data type */
		0					/* Data */
	);
}

void Texture::CreateTexture2D_Multisample(glm::ivec2 size, EFormat format /* = eRGBA */, EDataType dataType /* = eUByte */, GLsizei samples /* = 0 */)
{
	glTexImage2DMultisample(
		GL_TEXTURE_2D_MULTISAMPLE,	/* Target */
		samples,					/* Samples per fragment */
		format,						/* Texture pixel format */
		(GLsizei)size.x,			/* Data width */
		(GLsizei)size.y,			/* Data height */
		GL_TRUE						/* Fixed sample location */
	);
}

void Texture::CreateTexture2D(const Bitmap &bitmap)
{
	GLenum format = BppToPixelFormat(bitmap.GetBpp());
	glTexImage2D(
		GL_TEXTURE_2D,		/* Target */
		0,					/* Mipmap level */
		GL_RGBA,			/* Texture pixel format */
		bitmap.GetWidth(),	/* Data width */
		bitmap.GetHeight(),	/* Data height */
		0,					/* Border size */
		format,				/* Data format */
		GL_UNSIGNED_BYTE,	/* Data type */
		bitmap.GetData()	/* Data */
	);
}

void Texture::CreateCubeMap(const Texture::BitmapList &bitmaps)
{
	for (size_t i = 0; i < bitmaps.size(); i++) {
		const Bitmap &bitmap = *bitmaps.at(i).first;
		const ESide &side = bitmaps.at(i).second;
		GLenum format = BppToPixelFormat(bitmap.GetBpp());
		glTexImage2D(
			side,				/* Target */
			0,					/* Mipmap level */
			GL_RGBA,			/* Texture pixel format */
			bitmap.GetWidth(),	/* Data width */
			bitmap.GetHeight(),	/* Data height */
			0,					/* Border size */
			format,				/* Data format */
			GL_UNSIGNED_BYTE,	/* Data type */
			bitmap.GetData()	/* Data */
		);
	}
}

void Texture::CreateCubeMap(glm::ivec2 size, EFormat format, EDataType dataType)
{
	for (size_t i = 0; i < 6; i++) {
		glTexImage2D(
			(GLenum)(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i),		/* Target */
			0,					/* Mipmap level */
			format,				/* Texture pixel format */
			size.x,				/* Data width */
			size.y,				/* Data height */
			0,					/* Border size */
			format,				/* Data format */
			dataType,			/* Data type */
			0					/* Data */
		);
	}
}

GLenum Texture::BppToPixelFormat(int bpp)
{
	GLenum format;
	switch (bpp) {
	case 32: format = GL_BGRA; break;
	case 24: format = GL_BGR; break;
	default: format = GL_DEPTH_COMPONENT; break;
	}
	return format;
}