#pragma once

#include "config.h"

class Light {
public:
	enum EType {
		eDirectional,
		ePoint,
		eSpot
	};

public:
	glm::vec3 mAmbientColor;
	glm::vec3 mDiffuseColor;
	glm::vec3 mSpecularColor;

	glm::vec3 mPosition;
	glm::vec3 mDirection;

	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;

	EType mType;
};