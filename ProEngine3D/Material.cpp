#include "Material.h"

PRO_API void Material::AddTexture(ETextureType type, shared_ptr<Texture> texture)
{
	return mTextures.push_back(pair<ETextureType, shared_ptr<Texture>>(type, texture));
}