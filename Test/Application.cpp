#include <Logger.h>

#include "Application.h"

#include <iostream>

#pragma comment(lib, "ProEngine3D")
#ifdef _DEBUG
#pragma comment(lib, "sfml-window-d")
#pragma comment(lib, "sfml-system-d")
#else
#pragma comment(lib, "sfml-window")
#pragma comment(lib, "sfml-system")
#endif

#define _DEDICATED_GRAPHICS_CARD	0
#if _DEDICATED_GRAPHICS_CARD == 1
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}
#endif

using namespace std;

Application::Application() {
}

Application::~Application() {

}

void Application::Init() {
	/* Window Init */
	m_Window.create(sf::VideoMode(1920, 1080, 32), "ProEngine3D test", sf::Style::Default, sf::ContextSettings(24, 8, 4));
	sf::ContextSettings settings = m_Window.getSettings();
	cout << "[Info] OpenGL version " << settings.majorVersion << "." << settings.minorVersion << endl;

	ProEngine3D::Init();

	/* OpenGL Init */
	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClearDepth(1.0f);

	mShaders["Lighting"] = make_shared<Shader>("Shaders/Lighting");
	mShaders["StaticColor"] = make_shared<Shader>("Shaders/StaticColor");
	mShaders["NormalMapping"] = make_shared<Shader>("Shaders/NormalMapping");
	mShaders["PhongLighting"] = make_shared<Shader>("Shaders/PhongLighting");
	mShaders["ParallaxMapping"] = make_shared<Shader>("Shaders/ParallaxMapping");
	mShaders["SkyBox"] = make_shared<Shader>("Shaders/SkyBox");
	mShaders["Postprocess"] = make_shared<Shader>("Shaders/Postprocess");
	mShaders["Postprocess_ms"] = make_shared<Shader>("Shaders/Postprocess_ms");
	mShaders["ShadowMapping"] = make_shared<Shader>("Shaders/ShadowMapping");
	mShaders["PointShadows"] = make_shared<Shader>("Shaders/PointShadows");

	mCamera.SetPosition(vec3(40.0f, 150.0f, 180.0f));
	mCamera.SetLookAt(vec3(0.0f, 20.0f, 0.0f));

	mModelMatrix = mat4(1.0f);
	//mModelMatrix = rotate(mModelMatrix, radians(70.0f), vec3(0.0f, 1.0f, 0.0f));

	mDirectionalLight.mType = Light::eDirectional;
	mDirectionalLight.mAmbientColor = vec3(0.1f);
	mDirectionalLight.mDiffuseColor = vec3(0.4f);
	mDirectionalLight.mSpecularColor = vec3(0.0f);
	mDirectionalLight.mDirection = normalize(vec3(1.0f, -1.0f, -1.0f));

	mPointLight.mType = Light::ePoint;
	mPointLight.mAmbientColor = vec3(0.2f);
	mPointLight.mDiffuseColor = vec3(1.0f);
	mPointLight.mSpecularColor = vec3(1.0f);
	mPointLight.mPosition = vec3(30.0f, 100.0f, 10.0f);
	mPointLight.mConstAttenuation = 1.0f;
	mPointLight.mLinearAttenuation = 0.001f;
	mPointLight.mQuadraticAttenuation = 0.00005f;

	mFrameBuffer = make_unique<FrameBuffer>();
	vec2 windowSize = vec2(m_Window.getSize().x, m_Window.getSize().y);
	mColorBuffer = make_shared<Texture>();
	mColorBuffer->Create(Texture::eTexture2D_Multisample, windowSize, Texture::eRGB, Texture::eUByte, 4);
	mFrameBuffer->AttachColorBuffer(*mColorBuffer);
	mFrameBuffer->AttachDepthStencilBuffer(windowSize, 4);

	mShadowFrame = make_unique<FrameBuffer>();
	mShadowTexture = make_shared<Texture>();
	mShadowTexture->Create(Texture::eTexture2D, windowSize, Texture::eDepth, Texture::eFloat);
	mShadowFrame->AttachDepthBuffer(*mShadowTexture);
	mShadowFrame->Bind();
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		Logger::Error("Framebuffer error");

	mShadowCubeFrame = make_unique<FrameBuffer>();
	mShadowCubeTexture = make_shared<Texture>();
	mShadowCubeTexture->Create(Texture::eCubeMap, vec2(windowSize.y), Texture::eDepth, Texture::eFloat);
	mShadowCubeFrame->AttachDepthBuffer(*mShadowCubeTexture);

	LoadModels();

	Resize(m_Window.getSize().x, m_Window.getSize().y);
}

void Application::Resize(uint_t width, uint_t height) {
	mCamera.SetPerspective(50.0f, width / (float)height, 1.0f, 50000.0f);
	glViewport(0, 0, width, height);
}

void Application::HandleInput(sf::Event & e) {
	static bool leftButtonPressed = false;
	static vec2 mouseLastPos = vec2(0.0f);
	static float a = 0.0f;
	static float a2 = 0.0f;
	static float r = 200.0f;
	static vec3 p = vec3(0.0f);

	mat4 t = rotate(mat4(1.0f), a, vec3(0.0f, 1.0f, 0.0f));
	t = rotate(t, a2, vec3(1.0f, 0.0f, 0.0f));

	switch (e.type) {
	case sf::Event::MouseButtonPressed:
		if (e.mouseButton.button == sf::Mouse::Button::Left)
			leftButtonPressed = true;
		break;

	case sf::Event::MouseButtonReleased:
		if (e.mouseButton.button == sf::Mouse::Button::Left)
			leftButtonPressed = false;
		break;

	case sf::Event::MouseMoved:
		if (leftButtonPressed) {
			a += (e.mouseMove.x - mouseLastPos.x) / 100.0f;
			a2 -= (e.mouseMove.y - mouseLastPos.y) / 150.0f;
		}
		mouseLastPos = vec2(e.mouseMove.x, e.mouseMove.y);
		break;

	case sf::Event::MouseWheelScrolled:
		r -= e.mouseWheelScroll.delta*10;
		break;

	case sf::Event::KeyPressed:
		if (e.key.code == sf::Keyboard::Escape)
			mIsRunning = false;

		if (e.key.code == sf::Keyboard::Space)
			mOption = (mOption + 1) % 4;

		switch (e.key.code) {
		case sf::Keyboard::Left: p += vec3(t * vec4(-3.0f, 0.0f, 0.0f, 1.0f)); break;
		case sf::Keyboard::Right: p += vec3(t * vec4(3.0f, 0.0f, 0.0f, 1.0f)); break;
		case sf::Keyboard::Up: p += vec3(t * vec4(0.0f, 0.0f, -3.0f, 1.0f)); break;
		case sf::Keyboard::Down: p += vec3(t * vec4(0.0f, 0.0f, 3.0f, 1.0f)); break;
		}
		break;
	}

	/* Update camera transformation */
	t = rotate(mat4(1.0f), a, vec3(0.0f, 1.0f, 0.0f));
	t = rotate(t, a2, vec3(1.0f, 0.0f, 0.0f));
	mCamera.SetPosition(vec3(t * vec4(0.0f, 0.0f, r, 0.0f)) + p);
	mCamera.SetLookAt(p);
}

void Application::Update(double dt) {
	//mCamera.SetPosition(rotate(glm::mat4(1.0f), pi<float>() / 20.0f * (float)dt, vec3(0.0f, 1.0f, 0.0f)) * vec4(mCamera.GetPosition(), 0.0f));
	//mModelMatrix = rotate(mModelMatrix, pi<float>() / 12.0f * (float)dt, vec3(0.0f, 1.0f, 0.0f));

	mPointLight.mPosition = rotate(mat4(1.0f), -pi<float>() / 1.0f * (float)dt, vec3(0.0f, 1.0f, 0.0f)) * vec4(mPointLight.mPosition, 0.0);

	mScreenCut += pi<float>() / 3.0f * (float)dt;

	mFpsTime += (float)dt;
	if (mFpsTime >= 1.0f) {
		m_Window.setTitle("ProEngine3D | Fps: " + to_string(mFps));
		mFps = 0;
		mFpsTime -= 1.0;
	}
	mFps++;
}

void Application::Render() {
	// Shadow map
	// point lights
	glViewport(0, 0, m_Window.getSize().y, m_Window.getSize().y);
	mShadowCubeFrame->Bind();
	glClear(GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.05f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	RenderScene(eRenderPointShadows);

	// directional lights
	glViewport(0, 0, m_Window.getSize().x, m_Window.getSize().y);
	mShadowCamera = mCamera;
	mCamera.SetPosition(-mDirectionalLight.mDirection * 500.0f);
	mCamera.SetLookAt(vec3(0.0f));
	mCamera.SetOrthogonal(500.0f*m_Window.getSize().x / m_Window.getSize().y, 500.0f, 20000.0f);

	mShadowFrame->Bind();
	glClear(GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.05f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	RenderScene(eRenderShadows);

	swap(mCamera, mShadowCamera);

	// Scene
	glViewport(0, 0, m_Window.getSize().x, m_Window.getSize().y);

	mFrameBuffer->Bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.05f);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	RenderScene(eRenderScene);

	// Postprocessing
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	if (mColorBuffer->GetTextureType() == Texture::eTexture2D_Multisample)
		mActiveShader = mShaders["Postprocess_ms"];
	else
		mActiveShader = mShaders["Postprocess"];
	mActiveShader->Use();
	mActiveShader->Uniform1i("uDiffuseTexture", Material::eDiffuseMap);
	mActiveShader->Uniform2f("uWindowSize", (float)m_Window.getSize().x, (float)m_Window.getSize().y);

	mQuadModel->Render(mActiveShader.get());

	// Finish rednering
	m_Window.display();
}

void Application::Run() {
	Application *app = new Application;
	app->mIsRunning = true;
	app->Init();
	sf::Event e;
	sf::Clock clock;
	double dt = 0.0f;
	while (app->mIsRunning) {
		/* Handle Input */
		while (app->m_Window.pollEvent(e)) {
			switch (e.type) {
			case sf::Event::EventType::Closed:
				app->mIsRunning = false;
				break;

			case sf::Event::EventType::Resized:
				app->Resize(e.size.width, e.size.height);
				break;

			case sf::Event::EventType::JoystickButtonPressed:
			case sf::Event::EventType::JoystickButtonReleased:
			case sf::Event::EventType::JoystickMoved:
			case sf::Event::EventType::KeyPressed:
			case sf::Event::EventType::KeyReleased:
			case sf::Event::EventType::MouseButtonPressed:
			case sf::Event::EventType::MouseButtonReleased:
			case sf::Event::EventType::MouseMoved:
			case sf::Event::EventType::MouseWheelMoved:
			case sf::Event::EventType::MouseWheelScrolled:
				app->HandleInput(e);
				break;
			}
		}
		/* Update */
		app->Update(dt);
		/* Render */
		app->Render();

		//sf::sleep(sf::milliseconds(10) - clock.getElapsedTime());

		/* Calculate delta time */
		dt = clock.restart().asSeconds();
	}
}

void Application::RenderScene(ERenderMode render)
{
	if (render == eRenderShadows)
		mActiveShader = mShaders["StaticColor"];
	else if (render == eRenderPointShadows)
		mActiveShader = mShaders["PointShadows"];
	else
		mActiveShader = mShaders["ShadowMapping"];
	mActiveShader->Use();
	mActiveShader->UniformMatrix4fv("uShadowMatrix", false, &mShadowCamera.GetMatrix()[0][0]);
	glm::mat4 p = glm::perspective(glm::radians(90.0f), 1.0f, 1.0f, 1000.0f);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[0]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[1]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[2]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[3]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[4]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uPointShadowMatrix[5]", false, &(p * glm::lookAt(mPointLight.mPosition, mPointLight.mPosition + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)))[0][0]);
	mActiveShader->UniformMatrix4fv("uProjectionViewMatrix", false, &mCamera.GetMatrix()[0][0]);
	mActiveShader->Uniform3fv("uCameraPosition", &mCamera.GetPosition()[0]);
	mActiveShader->Uniform1i("uDiffuseTexture", Material::eDiffuseMap);
	mActiveShader->Uniform1i("uNormalTexture", Material::eNormalMap);
	mActiveShader->Uniform1i("uSpecularTexture", Material::eSpecularMap);
	mActiveShader->Uniform1i("uDisplacementTexture", Material::eDisplacementMap);
	mActiveShader->Uniform1i("uShadowTexture", Material::eShadowMap);
	mActiveShader->Uniform1i("uShadowCubeTexture", Material::eShadowCubeMap);
	mActiveShader->Uniform1i("uCubeTexture", Material::eCubeMap);
	mActiveShader->UniformLight("uDirLight", mDirectionalLight);
	mActiveShader->UniformLight("uPointLight", mPointLight);

	mShadowTexture->Bind(Material::eShadowMap);
	mShadowCubeTexture->Bind(Material::eShadowCubeMap);

	//mLamboModel->Render(mActiveShader.get());
	//mDragonModel->Render(mActiveShader.get());
	mPlaneModel->Render(mActiveShader.get());
	//mPokemonModel->Render(mActiveShader.get());
	//mHarleyModel->Render(mActiveShader.get(), translate(mat4(1.0f), vec3(20.0f, 0.0f, 0.0f)));
	mYamahaR1Model->Render(mActiveShader.get(), rotate(translate(mat4(1.0f), vec3(-100.0f, 0.0f, 0.0f)), radians(10.0f), vec3(0.0f, 1.0f, 0.0f)));
	mYamahaR1Model->Render(mActiveShader.get(), rotate(translate(mat4(1.0f), vec3(100.0f, 0.0f, 0.0f)), radians(-10.0f), vec3(0.0f, 1.0f, 0.0f)));
	mYamahaR1Model->Render(mActiveShader.get(), rotate(translate(mat4(1.0f), vec3(0.0f, 0.0f, -100.0f)), radians(100.0f), vec3(0.0f, 1.0f, 0.0f)));
	mYamahaR1Model->Render(mActiveShader.get(), rotate(translate(mat4(1.0f), vec3(0.0f, 0.0f, 100.0f)), radians(80.0f), vec3(0.0f, 1.0f, 0.0f)));
	//mHondaCBRModel->Render(mActiveShader.get(), rotate(translate(mat4(1.0f), vec3(25.0f, 0.0f, 0.0f)), radians(-10.0f), vec3(0.0f, 1.0f, 0.0f)));

	if (render != eRenderPointShadows) {
		mActiveShader = mShaders["StaticColor"];
		mActiveShader->Use();
		mActiveShader->UniformMatrix4fv("uProjectionViewMatrix", false, &mCamera.GetMatrix()[0][0]);
		mActiveShader->Uniform3fv("uColor", &mPointLight.mDiffuseColor[0]);

		mSphereModel->Render(mActiveShader.get(), translate(mat4(1.0f), mPointLight.mPosition));
	}

	if (render == eRenderScene) {
		mActiveShader = mShaders["SkyBox"];
		mActiveShader->Use();
		mActiveShader->UniformMatrix4fv("uProjectionViewMatrix", false, &mCamera.GetMatrix()[0][0]);
		mActiveShader->Uniform1i("uDiffuseTexture", Material::eDiffuseMap);
		mActiveShader->Uniform1i("uCubeTexture", Material::eCubeMap);
		mActiveShader->Uniform1i("uShadowCubeTexture", Material::eShadowCubeMap);
		mShadowCubeTexture->Bind(Material::eShadowCubeMap);

		mBoxModel->Render(mActiveShader.get(), translate(mat4(1.0f), vec3(0.0f, 10.0f, 0.0f)));
		mCubeModel->Render(mActiveShader.get());
	}
}

void Application::LoadModels()
{
	/* Drag.fbx */
	//mDragonModel = make_unique<Model>();
	//mDragonModel->LoadSourceFromFile("Models/drag.fbx");
	//mDragonModel->GetMaterials().at(0)->mColorSpecular = vec3(1.0f);
	//mDragonModel->GetMaterials().at(0)->mShininess = 24.0f;
	//mDragonModel->GetMaterials().at(0)->mShininessStrength = 1.0f;

	/* Pokemon */
	//mPokemonModel = make_unique<Model>();
	//mPokemonModel->LoadSourceFromFile("Models/pokemon/pokemon.fbx");
	//mPokemonModel->GetRootNode()->SetTransformation(scale(mPokemonModel->GetRootNode()->GetTransformation(), vec3(0.5f)));
	//mPokemonModel->GetMaterials().at(0)->AddTexture(Material::eDiffuseMap, make_shared<Texture>("Models/pokemon/Final_Pokemon_Diffuse.jpg"));
	//mPokemonModel->GetMaterials().at(0)->AddTexture(Material::eNormalMap, make_shared<Texture>("Models/pokemon/Final_Pokemon_Normal.jpg"));
	//mPokemonModel->GetMaterials().at(0)->AddTexture(Material::eSpecularMap, make_shared<Texture>("Models/pokemon/Final_Pokemon_Specular.jpg"));
	//mPokemonModel->GetMaterials().at(0)->mColorSpecular = vec3(0.1f, 0.1f, 0.1f);
	//mPokemonModel->GetMaterials().at(0)->mColorDiffuse = vec3(0.9f, 0.9f, 0.9f);
	//mPokemonModel->GetMaterials().at(0)->mColorAmbient = vec3(0.4f, 0.4f, 0.4f);

	/* Spehre fbx */
	mSphereModel = make_unique<Model>();
	mSphereModel->LoadFromFile("Models/sphere.fbx");
	mSphereModel->GetRootNode()->SetTransformation(scale(mat4(1.0f), vec3(0.05f)));

	/* Lamborghini_Aventador.fbx */
	//mLamboModel = make_unique<Model>();
	//mLamboModel->LoadSourceFromFile("Models/Lamborginhi Aventador FBX/Lamborghini_Aventador.fbx");
	//mLamboModel->GetRootNode()->SetTransformation(scale(mLamboModel->GetRootNode()->GetTransformation(), vec3(0.4f)));
	//mLamboModel->GetMaterials().at(0)->mReflectivity = 1.0f;
	//mLamboModel->GetMaterials().at(0)->AddTexture(Material::eCubeMap, make_shared<Texture>(Texture::PathList{
	//pair<string, Texture::ESide>("Textures/citadela/posz.jpg", Texture::eBack),
	//pair<string, Texture::ESide>("Textures/citadela/negy.jpg", Texture::eBottom),
	//pair<string, Texture::ESide>("Textures/citadela/negz.jpg", Texture::eFront),
	//pair<string, Texture::ESide>("Textures/citadela/negx.jpg", Texture::eLeft),
	//pair<string, Texture::ESide>("Textures/citadela/posx.jpg", Texture::eRight),
	//pair<string, Texture::ESide>("Textures/citadela/posy.jpg", Texture::eTop)
		//}));

	/* Plane */
	mPlaneModel = make_unique<Model>();
	mPlaneModel->GetMeshes().push_back(MeshBuilder::Plane());
	mPlaneModel->GetMeshes().at(0)->SetMaterialId(1);
	mPlaneModel->GetRootNode()->AddMesh(0);
	mPlaneModel->GetMaterials().push_back(make_unique<Material>());
	mPlaneModel->GetMaterials().push_back(make_unique<Material>());
	mPlaneModel->GetMaterials().push_back(make_unique<Material>());

	mPlaneModel->GetMaterials().at(0)->AddTexture(Material::eDiffuseMap, make_shared<Texture>("Textures/Bricks/Bricks2.jpg"));
	mPlaneModel->GetMaterials().at(0)->AddTexture(Material::eNormalMap, make_shared<Texture>("Textures/Bricks/Bricks2_normal.jpg"));
	mPlaneModel->GetMaterials().at(0)->AddTexture(Material::eDisplacementMap, make_shared<Texture>("Textures/Bricks/Bricks2_disp.jpg"));

	mPlaneModel->GetMaterials().at(1)->AddTexture(Material::eDiffuseMap, make_shared<Texture>("Textures/Stone_02/Stone_02_COLOR.tga"));
	mPlaneModel->GetMaterials().at(1)->AddTexture(Material::eNormalMap, make_shared<Texture>("Textures/Stone_02/Stone_02_NRM.tga"));
	mPlaneModel->GetMaterials().at(1)->AddTexture(Material::eSpecularMap, make_shared<Texture>("Textures/Stone_02/Stone_02_SPEC.tga"));
	mPlaneModel->GetMaterials().at(1)->AddTexture(Material::eDisplacementMap, make_shared<Texture>("Textures/Stone_02/Stone_02_DISP.tga"));

	mPlaneModel->GetMaterials().at(2)->AddTexture(Material::eDiffuseMap, make_shared<Texture>("Textures/Tiles_11_SD/Tiles_011_COLOR.jpg"));
	mPlaneModel->GetMaterials().at(2)->AddTexture(Material::eNormalMap, make_shared<Texture>("Textures/Tiles_11_SD/Tiles_011_NORM.jpg"));
	mPlaneModel->GetMaterials().at(2)->AddTexture(Material::eSpecularMap, make_shared<Texture>("Textures/Tiles_11_SD/Tiles_011_ROUGH.jpg"));
	mPlaneModel->GetMaterials().at(2)->AddTexture(Material::eDisplacementMap, make_shared<Texture>("Textures/Tiles_11_SD/Tiles_011_DISP.png"));

	mPlaneModel->GetMaterials().at(0)->mShininess = 16.0f;
	mat4 t = mat4(1.0f);
	t = rotate(t, -pi<float>() / 2.0f, vec3(1.0f, 0.0f, 0.0f));
	t = scale(t, vec3(500.0f));
	mPlaneModel->GetRootNode()->SetTransformation(t);
	t = scale(mat4(1.0f), vec3(20.0f));
	mPlaneModel->GetRootNode()->SetTextureTransformation(t);

	/* Cube */
	mCubeModel = make_unique<Model>();
	mCubeModel->GetMeshes().push_back(MeshBuilder::Cube());
	mCubeModel->GetMeshes().at(0)->SetMaterialId(0);
	mCubeModel->GetMaterials().push_back(make_unique<Material>());
	mCubeModel->GetMaterials().at(0)->AddTexture(Material::eCubeMap, make_shared<Texture>(Texture::PathList{
		pair<string, Texture::ESide>("Textures/citadela/posz.jpg", Texture::eBack),
		pair<string, Texture::ESide>("Textures/citadela/negy.jpg", Texture::eBottom),
		pair<string, Texture::ESide>("Textures/citadela/negz.jpg", Texture::eFront),
		pair<string, Texture::ESide>("Textures/citadela/negx.jpg", Texture::eLeft),
		pair<string, Texture::ESide>("Textures/citadela/posx.jpg", Texture::eRight),
		pair<string, Texture::ESide>("Textures/citadela/posy.jpg", Texture::eTop)
		}));
	t = mat4(1.0f);
	t = scale(t, vec3(10000.0f));
	mCubeModel->GetRootNode()->SetTransformation(t);
	mCubeModel->GetRootNode()->AddMesh(0);

	/* Quad */
	mQuadModel = make_unique<Model>();
	mQuadModel->GetMeshes().push_back(MeshBuilder::Plane());
	mQuadModel->GetMeshes().at(0)->SetMaterialId(0);
	mQuadModel->GetRootNode()->AddMesh(0);
	mQuadModel->GetMaterials().push_back(make_unique<Material>());
	mQuadModel->GetMaterials().at(0)->AddTexture(Material::eDiffuseMap, mColorBuffer);

	/* Yamaha R1 */
	mYamahaR1Model = make_unique<Model>();
	mYamahaR1Model->LoadFromFile("Models/Yamaha R1/R1.FBX");
	mYamahaR1Model->GetRootNode()->SetTransformation(scale(mYamahaR1Model->GetRootNode()->GetTransformation(), vec3(20.0f)));

	///* Harley Davidson */
	//mHarleyModel = make_unique<Model>();
	//mHarleyModel->LoadSourceFromFile("Models/harley/harley.dae");
	//mHarleyModel->GetRootNode()->SetTransformation(scale(mHarleyModel->GetRootNode()->GetTransformation(), vec3(0.9f)));

	///* Honda CBR */
	//mHondaCBRModel = make_unique<Model>();
	//mHondaCBRModel->LoadSourceFromFile("Models/CBR/cbr.fbx");
	//mHondaCBRModel->GetRootNode()->SetTransformation(scale(mHondaCBRModel->GetRootNode()->GetTransformation(), vec3(43.0f)));

	/* Box */
	mBoxModel = make_unique<Model>();
	mBoxModel->GetMeshes().push_back(MeshBuilder::Cube());
	mBoxModel->GetMeshes().at(0)->SetMaterialId(0);
	mBoxModel->GetRootNode()->AddMesh(0);
	mBoxModel->GetRootNode()->SetTransformation(scale(mat4(1.0f), vec3(10.0f)));
	mBoxModel->GetMaterials().push_back(make_unique<Material>());
	mBoxModel->GetMaterials().at(0)->AddTexture(Material::eCubeMap, mShadowCubeTexture);
}