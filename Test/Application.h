#pragma once

#include <map>
#include <memory>

#include <ProEngine3D.h>

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace glm;

using std::map;
using std::unique_ptr;
using std::make_unique;

class Application {
public:
	enum ERenderMode {
		eRenderScene,
		eRenderShadows,
		eRenderPointShadows
	};

private:
	sf::Window m_Window;
	map<string, shared_ptr<Shader>> mShaders;
	shared_ptr<Shader> mActiveShader;

	Camera mCamera, mShadowCamera;

	unique_ptr<FrameBuffer> mFrameBuffer;
	unique_ptr<FrameBuffer> mShadowFrame, mShadowCubeFrame;
	shared_ptr<Texture> mColorBuffer, mShadowTexture, mShadowCubeTexture;

	Light mDirectionalLight, mPointLight;

	unique_ptr<Model>
		mDragonModel,
		mPokemonModel,
		mBoxModel,
		mSphereModel,
		mLamboModel,
		mPlaneModel,
		mCubeModel,
		mQuadModel,
		mYamahaR1Model,
		mHarleyModel,
		mHondaCBRModel;

	mat4 mModelMatrix;

	float mScreenCut = 0.0f;
	int mOption = 0;

	bool mIsRunning = false;

	uint32_t mFps = 0;
	float mFpsTime = 0.0f;

private:
	Application();
public:
	~Application();

	void Init();
	void Resize(uint_t width, uint_t height);
	void HandleInput(sf::Event &e);
	void Update(double dt);
	void Render();

	static void Run();

private:
	void RenderScene(ERenderMode render);
	void LoadModels();
};