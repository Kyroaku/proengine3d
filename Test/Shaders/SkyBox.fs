#version 330 core

out vec4 FragColor;

uniform samplerCube uCubeTexture;

uniform int uHasCubeMap;

in vec3 vTexCoord;

void main()
{
	FragColor = vec4(texture(uCubeTexture, vTexCoord).rgb, 1.0);
} 