#version 330 core

out vec4 FragColor;

uniform sampler2DMS uDiffuseTexture;
uniform vec2 uWindowSize;

in vec3 vTexCoord;

vec3 textureMultisample(sampler2DMS t, ivec2 tc);

void main()
{
	ivec2 tc = ivec2(vTexCoord.xy * uWindowSize);
	vec3 color = textureMultisample(uDiffuseTexture, tc);

	FragColor = vec4(color, 1.0);
} 

vec3 textureMultisample(sampler2DMS t, ivec2 tc)
{
	vec3 color = vec3(0.0f);
	for(int i = 0; i < 4; i++)
		color += texelFetch(t, tc, i).rgb;
	return (color /= vec3(4.0f));
}