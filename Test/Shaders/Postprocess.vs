#version 330 core

layout (location = 0) in vec3 aPosition;

out vec3 vTexCoord;

void main()
{
	vTexCoord = aPosition / 2.0f + 0.5f;
    gl_Position = vec4(aPosition, 1.0);
}