#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vVS[];

out GS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vGS;  

void main()
{
	for(int i = 0; i < 3; i++) {
		vGS.vPosition = vVS[i].vPosition;
		vGS.vColor = vVS[i].vColor;
		vGS.vNormal = vVS[i].vNormal;
		vGS.vTexCoord = vVS[i].vTexCoord;
		vGS.vTangentRotation = vVS[i].vTangentRotation;
		gl_Position = gl_in[i].gl_Position; 
		EmitVertex();
	}
    
    EndPrimitive();
}