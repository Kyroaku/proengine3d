#version 330 core

struct Material {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	float mOpacity;
	float mShininess;
	float mShininessStrength;
	float mReflectivity;
};

struct Light {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	vec3 mVector;
	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;
};

out vec4 FragColor;

uniform vec3 uCameraPosition;

uniform int uHasDiffuseMap;
uniform int uHasNormalMap;
uniform int uHasSpecularMap;
uniform int uHasDisplacementMap;
uniform int uHasCubeMap;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uSpecularTexture;
uniform sampler2D uDisplacementTexture;
uniform samplerCube uCubeTexture;

uniform Material uMaterial;
uniform Light uDirLight;
uniform Light uPointLight;

uniform float uScreenCut;

in vec3 vPosition;
in vec4 vColor;
in vec3 vNormal;
in vec3 vTexCoord;
in mat3 vTangentRotation;

void main()
{
	vec3 texCoord = vTexCoord;
	if(uHasDisplacementMap == 1) {
		vec3 viewDir = normalize(vTangentRotation * (uCameraPosition - vPosition));
		vec3 tcStep = vec3(viewDir.xy * 0.1f, 1.0f) / 10.0f;
		float currentDepth = 0.0f;
		while(true) {
			float d = 1.0f - texture2D(uDisplacementTexture, texCoord.xy).r;
			if(d < currentDepth) {
				d = currentDepth - d;
				float d2 = 1.0f - texture2D(uDisplacementTexture, texCoord.xy-tcStep.xy).r - (currentDepth - tcStep.z);
				texCoord.xy = mix(texCoord.xy, texCoord.xy - tcStep.xy, d / (d+d2));
				break;
			}
			texCoord.xy += tcStep.xy;
			currentDepth += tcStep.z;
		}
	}

	vec3 diffuseMap = vec3(1.0f);
	if(uHasDiffuseMap == 1)
		diffuseMap = texture2D(uDiffuseTexture, texCoord.xy).rgb;

	vec3 normal = normalize(vNormal);
	if(uHasNormalMap == 1) {
		normal = texture2D(uNormalTexture, texCoord.xy).xyz * 2.0 - 1.0;
		normal = normalize(vTangentRotation * normal);
	}

	vec3 specularMap = vec3(1.0f);
	if(uHasSpecularMap == 1)
		specularMap *= texture2D(uSpecularTexture, texCoord.xy).rgb * uMaterial.mShininessStrength;

	vec3 ambientColor = vec3(0.0f);
	vec3 diffuseColor = vec3(0.0f);
	vec3 specularColor = vec3(0.0f);

	float diffuseFactor = 0.0f;
	float shininessFactor = 0.0f;
	vec3 camDir = normalize(uCameraPosition - vPosition);
	vec3 lightDir = normalize(vPosition - uPointLight.mVector);

	/* Directional lights */
	diffuseFactor = max(0.0, dot(-normal, uDirLight.mVector));
	shininessFactor = pow(max(0.0, dot(reflect(uDirLight.mVector, normal), camDir)), uMaterial.mShininess);
	ambientColor += uDirLight.mAmbientColor;
	diffuseColor += uDirLight.mDiffuseColor * diffuseFactor;
	specularColor += uDirLight.mSpecularColor * shininessFactor;

	/* Point lights */
	float distance = length(uPointLight.mVector - vPosition);
	diffuseFactor = max(0.0, dot(-normal, lightDir));
	shininessFactor = pow(max(0.0, dot(reflect(lightDir, normal), camDir)), uMaterial.mShininess);
	float attenuationFactor = 1.0f / (
		uPointLight.mConstAttenuation +
		uPointLight.mLinearAttenuation * distance +
		uPointLight.mQuadraticAttenuation * distance * distance
		);
	ambientColor += attenuationFactor * uPointLight.mAmbientColor;
	diffuseColor += attenuationFactor * uPointLight.mDiffuseColor * diffuseFactor;
	specularColor += attenuationFactor * uPointLight.mSpecularColor * shininessFactor;
	
	/* Reflections */
	float c = uScreenCut;
	float cut = uScreenCut + sin(gl_FragCoord.y * 0.05f + c * 0.01f) * 10.0f + sin(dot(normalize(vec3(1.0f, 0.0f, 2.0f)), normal) * c/500.0f) * 120.0f;
	if(uHasCubeMap == 1 && gl_FragCoord.x > cut) {
		vec3 reflectedViewDir = normalize(reflect(-camDir, normal));
		vec3 reflectColor = texture(uCubeTexture, reflectedViewDir).rgb;
		diffuseMap = mix(diffuseMap, reflectColor, uMaterial.mReflectivity);
	}

	/* Final color */
	vec3 color =
		ambientColor * uMaterial.mAmbientColor +
		diffuseColor * uMaterial.mDiffuseColor * diffuseMap +
		specularColor * uMaterial.mSpecularColor * specularMap;

	FragColor = vec4(color, uMaterial.mOpacity);
}