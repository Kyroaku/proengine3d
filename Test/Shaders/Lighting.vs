#version 330 core

layout (location = 0) in vec4 aPosition;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec3 aTexCoord;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in vec3 aTangent;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uModelMatrix;

out vec4 vColor;
out vec3 vTexCoord;
out vec3 vNormal;

void main()
{
	vColor = aColor;
	vTexCoord = aTexCoord;
	vNormal = normalize(transpose(inverse(mat3(uModelMatrix))) * aNormal);

    gl_Position = uProjectionViewMatrix * uModelMatrix * vec4(aPosition.xyz, 1.0);
}