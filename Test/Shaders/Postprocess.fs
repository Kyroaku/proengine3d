#version 330 core

out vec4 FragColor;

uniform sampler2D uDiffuseTexture;

in vec3 vTexCoord;

void main()
{
	vec3 color = vec3(0.0);

	float kernel[9] = float[](
		0,0,0,
		0,1,0,
		0,0,0
	);

	for(int i = -1; i < 2; i++)
		for(int j = -1; j < 2; j++)
			color += kernel[(i+1)*3 + (j+1)] * texture2D(uDiffuseTexture, vec2(vTexCoord.xy + vec2(i,j)*0.000926f)).rgb;
	color = color;
	FragColor = vec4(color, 1.0);
} 