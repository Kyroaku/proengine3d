#version 330 core

struct Light {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	vec3 mVector;
	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;
};

out vec4 FragColor;

uniform Light uPointLight;

in GS {
	vec3 vPosition;
} vGS; 

void main()
{
	gl_FragDepth = length(vGS.vPosition - uPointLight.mVector) / 1000.0f;
} 