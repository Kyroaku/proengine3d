#version 330 core

out vec4 FragColor;

in vec4 vPosition;
in vec4 vColor;

void main()
{
	//gl_FragDepth = vPosition.w;
	float p = 0.6;
	if(vPosition.w != 0.0)
		discard;
	FragColor = vec4(vColor.rgb, 1.0);
} 