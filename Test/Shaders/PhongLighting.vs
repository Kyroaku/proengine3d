#version 330 core

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec3 aTexCoord;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in vec3 aTangent;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uModelMatrix;

out vec3 vPosition;
out vec4 vColor;
out vec3 vNormal;
out vec3 vTexCoord;
out mat3 vTangentRotation;

const float M_PI = 3.141592;

void main()
{
	vTangentRotation = mat3(uModelMatrix) * mat3(aTangent, cross(aNormal, aTangent), aNormal);

	vPosition = vec3(uModelMatrix * vec4(aPosition, 1.0f));
	vColor = aColor;
	vNormal = normalize(transpose(inverse(mat3(uModelMatrix))) * aNormal);
	vTexCoord = aTexCoord;

    gl_Position = uProjectionViewMatrix * uModelMatrix * vec4(aPosition, 1.0);
}