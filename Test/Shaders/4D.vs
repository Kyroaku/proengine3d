#version 330 core

layout (location = 0) in vec4 aPosition;
layout (location = 1) in vec4 aColor;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uModelMatrix;

out vec4 vPosition;
out vec4 vColor;

void main()
{
	vec4 position = aPosition;
	vColor = vec4(vec3(1.0f-position.w), 1.0);
	vPosition = position;
	position *= 1.0 / (position.w + 1);
    gl_Position = uProjectionViewMatrix * uModelMatrix * vec4(position.xyz, 1.0);
}