#version 330 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 18) out;

uniform mat4 uPointShadowMatrix[6];

out GS {
	vec3 vPosition;
} vGS; 

void main() {    
	for(int i = 0; i < 6; i++) {
		gl_Layer = i;

		for(int j = 0; j < 3; j++) {
			gl_Position = uPointShadowMatrix[i] * gl_in[j].gl_Position;
			vGS.vPosition = gl_in[j].gl_Position.xyz;
			EmitVertex();
		}

		EndPrimitive();
	}
} 