#version 330 core

struct Light {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	vec3 mVector;
	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;
};

out vec4 FragColor;

uniform int uHasDiffuseMap;
uniform int uHasNormalMap;

uniform vec3 uCameraPosition;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;

uniform Light uPointLight;

in VS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vVS;

void main()
{
	vec3 texCoord = vVS.vTexCoord;

	vec3 diffuseMap = texture2D(uDiffuseTexture, texCoord.xy).xyz;
	vec3 normalTexture = texture2D(uNormalTexture, texCoord.xy).xyz;

	vec3 normal = normalize(vVS.vNormal);
	if(uHasNormalMap == 1) {
		normal = texture2D(uNormalTexture, texCoord.xy).xyz * 2.0 - 1.0;
		normal = normalize(vVS.vTangentRotation * normal);
	}

	vec3 ambientColor = vec3(0.0f);
	vec3 diffuseColor = vec3(0.0f);
	vec3 specularColor = vec3(0.0f);

	float diffuseFactor = 0.0f;
	float shininessFactor = 0.0f;
	vec3 camDir = normalize(uCameraPosition - vVS.vPosition);
	vec3 lightDir = normalize(vVS.vPosition - uPointLight.mVector);

	/* Point lights */
	float distance = length(uPointLight.mVector - vVS.vPosition);
	diffuseFactor = max(0.0, dot(-normal, lightDir));
	//shininessFactor = pow(max(0.0, dot(reflect(lightDir, normal), camDir)), uMaterial.mShininess);
	float attenuationFactor = 1.0f / (
		uPointLight.mConstAttenuation +
		uPointLight.mLinearAttenuation * distance +
		uPointLight.mQuadraticAttenuation * distance * distance
		);
	ambientColor += attenuationFactor * uPointLight.mAmbientColor;
	diffuseColor += attenuationFactor * uPointLight.mDiffuseColor * diffuseFactor;
	specularColor += attenuationFactor * uPointLight.mSpecularColor * shininessFactor;

	/* Final color */
	vec3 color =
		ambientColor +
		diffuseColor * diffuseMap * vVS.vPosition +
		specularColor;

	FragColor = vec4(color, 1.0);
} 