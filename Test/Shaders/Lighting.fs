#version 330 core

out vec4 FragColor;

uniform sampler2D uDiffuseTexture;

uniform int uHasDiffuseMap;

uniform vec3 uAmbientColor;
uniform vec3 uDiffuseColor;
uniform vec3 uSpecularColor;
uniform float uOpacity;

in vec4 vColor;
in vec3 vTexCoord;
in vec3 vNormal;

void main()
{
	vec3 lightDir = normalize(vec3(-0.1, -0.3, -1.0));

	vec3 diffuseTexture = texture2D(uDiffuseTexture, vTexCoord.xy).rgb;

	float diffuse = clamp(dot(-vNormal, lightDir), 0.0, 1.0);

	vec3 color = vec3(1.0f);
	if(uHasDiffuseMap == 1)
		color *= diffuseTexture;

	FragColor = vec4(color * diffuse, uOpacity);
} 