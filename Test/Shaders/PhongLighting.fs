#version 330 core

struct Material {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	float mOpacity;
	float mShininess;
	float mShininessStrength;
};

struct Light {
	vec3 mAmbientColor;
	vec3 mDiffuseColor;
	vec3 mSpecularColor;
	vec3 mVector;
	float mConstAttenuation;
	float mLinearAttenuation;
	float mQuadraticAttenuation;
};

out vec4 FragColor;

uniform vec3 uCameraPosition;

uniform int uHasDiffuseMap;
uniform int uHasNormalMap;
uniform int uHasSpecularMap;

uniform sampler2D uDiffuseTexture;
uniform sampler2D uNormalTexture;
uniform sampler2D uSpecularTexture;

uniform Material uMaterial;
uniform Light uDirLight;
uniform Light uPointLight;

in vec3 vPosition;
in vec4 vColor;
in vec3 vNormal;
in vec3 vTexCoord;
in mat3 vTangentRotation;

void main()
{
	vec3 diffuseMap = vec3(1.0f);
	if(uHasDiffuseMap == 1)
		diffuseMap = texture2D(uDiffuseTexture, vTexCoord.xy).xyz;

	vec3 normal = vNormal;
	if(uHasNormalMap == 1) {
		normal = texture2D(uNormalTexture, vTexCoord.xy).xyz * 2.0 - 1.0;
		normal = normalize(vTangentRotation * normal);
	}

	vec3 specularMap = vec3(uMaterial.mShininessStrength);
	if(uHasSpecularMap == 1)
		specularMap *= texture2D(uSpecularTexture, vTexCoord.xy).xyz;

	vec3 ambientColor = vec3(0.0f);
	vec3 diffuseColor = vec3(0.0f);
	vec3 specularColor = vec3(0.0f);

	float diffuseFactor = 0.0f;
	float shininessFactor = 0.0f;
	vec3 camDir = normalize(uCameraPosition - vPosition);
	vec3 lightDir = normalize(vPosition - uPointLight.mVector);

	/* Directional lights */
	diffuseFactor = max(0.0, dot(-normal, uDirLight.mVector));
	shininessFactor = pow(max(0.0, dot(reflect(uDirLight.mVector, normal), camDir)), uMaterial.mShininess);
	ambientColor += uDirLight.mAmbientColor;
	diffuseColor += uDirLight.mDiffuseColor * diffuseFactor;
	specularColor += uDirLight.mSpecularColor * shininessFactor;

	/* Point lights */
	float distance = length(uPointLight.mVector - vPosition);
	diffuseFactor = max(0.0, dot(-normal, lightDir));
	shininessFactor = pow(max(0.0, dot(reflect(lightDir, normal), camDir)), uMaterial.mShininess);
	float attenuationFactor = 1.0f / (
		uPointLight.mConstAttenuation +
		uPointLight.mLinearAttenuation * distance +
		uPointLight.mQuadraticAttenuation * distance * distance
		);
	ambientColor += attenuationFactor * uPointLight.mAmbientColor;
	diffuseColor += attenuationFactor * uPointLight.mDiffuseColor * diffuseFactor;
	specularColor += attenuationFactor * uPointLight.mSpecularColor * shininessFactor;

	/* Final color */
	vec3 color =
		ambientColor * uMaterial.mAmbientColor +
		diffuseColor * uMaterial.mDiffuseColor * diffuseMap +
		specularColor * uMaterial.mSpecularColor * specularMap;

	FragColor = vec4(color, uMaterial.mOpacity);
}