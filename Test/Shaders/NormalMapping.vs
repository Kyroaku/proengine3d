#version 330 core

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec3 aTexCoord;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in vec3 aTangent;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uModelMatrix;
uniform mat4 uTextureMatrix;

out VS {
	vec3 vPosition;
	vec4 vColor;
	vec3 vNormal;
	vec3 vTexCoord;
	mat3 vTangentRotation;
} vVS;

void main()
{
	mat3 normalMatrix = transpose(inverse(mat3(uModelMatrix)));

	vVS.vTangentRotation = normalMatrix * mat3(aTangent, cross(aNormal, aTangent), aNormal);

	vVS.vPosition = vec3(uModelMatrix * vec4(aPosition, 1.0f));
	vVS.vColor = aColor;
	vVS.vNormal = normalMatrix * aNormal;
	vVS.vTexCoord = vec3(uTextureMatrix * vec4(aTexCoord, 1.0f));

    gl_Position = uProjectionViewMatrix * uModelMatrix * vec4(aPosition, 1.0);
}