#version 330 core

layout (location = 0) in vec3 aPosition;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uModelMatrix;

out vec3 vTexCoord;

void main()
{
	vTexCoord = aPosition;
    gl_Position = uProjectionViewMatrix * uModelMatrix * vec4(aPosition, 1.0);
}